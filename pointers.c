#include<stdio.h>
int input()
{
    int a;
    printf("enter a number=");
    scanf("%d",&a);
    return a;
}
int swap(int *x,int *y)
{
    *x = *x + *y;
    *y = *x - *y;
    *x = *x - *y;
    return 0;
}
void output(int a,int b)
{
    printf("\n the number after swapping %d and %d \n",a,b);
}
int main()
{
    int x,y,*a,*b;
    x=input();
    y=input();
    printf("\n the number before swapping=%d and %d \n",x,y);
    a=&x;
    b=&y;
    swap(a,b);
    output(x,y);
    return 0;
}